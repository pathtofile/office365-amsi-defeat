# AMSI defeat for office 365
This is an example word doc that bypasses Office365 AMSI logging

# Office 365 and AMSI
Office 365 only passes calls to certain Win32 calls to AMSI, instead of the full script run, as PowerShell does.

Furthermore, by default, Office does not log and macros run from a template file that exists in the `template` directories.

# Defeat
This example doc does two thing:
1. Copy itself the users `template` folder
2. Load up a new Application, openinig up the new template, and executing another function in it that runs `shell(calc.exe)`

Nothing should be passed to AMSI at all. Tested OCT 2018, same day it got released to the public.